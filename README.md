# Resume
This whole project has only one purpose: being an HTML-built resume.
All personal data have are cleared out of it, just to be set properly.

## Installation
1. Set the right desired values in `docker-compose.yaml`, under the environnement settings
2. Copy the file `public/env/parameters.json.dist` under `public/env/parameters.json` and set the proper values
3. The web server is running under Basic authentication and must be set under environment variables described in `docker-compose.yaml`

## Ansible
Runs the following command to setup a server and start it vanilla:
```bash
cd ansible/
ansible-playbook deploy.yml -u $REMOTE_USER -k
```
