# How to use

## Installing requirements
```bash
ansible-galaxy install -r requirements.yml
```

## Running playbook
```bash
ansible-playbook playbook.yml -u $USER -k
```
