#!/bin/sh

htpasswd -b -c /usr/share/nginx/.htpasswd ${BASIC_AUTH_USR} ${BASIC_AUTH_PWD}
certbot -n --nginx -d ${NGINX_HOST} --agree-tos --email=${RENEW_EMAIL}

echo -e "#!/bin/sh\ncertbot renew" >> /etc/periodic/weekly/certbot-renew.sh
chmod +x /etc/periodic/weekly/certbot-renew.sh

nginx -s stop
sleep 0.25
