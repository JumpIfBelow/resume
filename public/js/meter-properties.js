(() => {
    const meters = Array.from(document.querySelectorAll('meter'));
    const meterAttributes = [
        'min',
        'low',
        'high',
        'optimum',
        'max',
        'value',
    ];

    meters.forEach(meter => {
        const style = meter.style;
        const percent = 100 / (meter.max - meter.min);

        meterAttributes.forEach(attribute => {
            style.setProperty(`--meter-${attribute}`, meter[attribute]);
            style.setProperty(`--meter-${attribute}-percent`, `${(meter[attribute] - meter.min) * percent}%`)
        });
    });
})();
