fetch('/env/parameters.json')
    .then(response => response.json())
    .then(parameters => {
        const meta = document.querySelector('#meta');

        const titleElement = document.querySelector('title');
        const firstNameElement = meta.querySelector('.first-name');
        const lastNameElement = meta.querySelector('.last-name');
        const addressElement = meta.querySelector('address');
        const phoneElement = meta.querySelector('.phone > a');
        const emailElement = meta.querySelector('.email > a');
        const ageElement = meta.querySelector('.age');

        titleElement.textContent = `${parameters.firstName} ${parameters.lastName}'s résumé`;
        firstNameElement.innerHTML = 'First name: ' + parameters.firstName + firstNameElement.innerHTML;
        lastNameElement.innerHTML = 'Last name: ' + parameters.lastName + lastNameElement.innerHTML;
        addressElement.innerHTML = parameters.address.join('<br>');

        const phoneNumber = parameters.phone;
        phoneElement.href = `tel:${phoneNumber}`;
        phoneElement.textContent = [
                [0, 3],
                [3, 4],
                [4, 6],
                [6, 8],
                [8, 10],
                [10, 12],
            ]
            .map(slices => phoneNumber.slice(slices[0], slices[1]))
            .join(' ')
        ;

        const emailAddress = parameters.email;
        emailElement.href = `mailto:${emailAddress}`;
        emailElement.textContent = emailAddress;

        const computeAge = (date) => {
            const birthday = new Date(date);
            const today = new Date();

            return (today - birthday) / 365 / 24 / 60 / 60 / 1000;
        };

        const birthday = parameters.birthday;

        const age = Math.floor(computeAge(birthday));

        ageElement.innerHTML = `<time datetime="${birthday}">${age}</time> years old`;
        const url = 'http://ipa-reader.xyz';
        const locationVoice = {
            'fr-FR': 'Mathieu',
            'en-US': 'Joey',
            'en-EN': 'Brian',
        };

        const firstNamePronunciationElement = firstNameElement.querySelector('.pronunciation');
        const lastNamePronunciationElement = lastNameElement.querySelector('.pronunciation');

        firstNamePronunciationElement.textContent = parameters.firstNamePronunciation;
        lastNamePronunciationElement.textContent = parameters.lastNamePronunciation;

        document
            .querySelectorAll('a.pronunciation[lang]')
            .forEach(anchorElement => {
                const parameters = {
                    voice: locationVoice[anchorElement.lang] || locationVoice['fr-FR'],
                    text: anchorElement.textContent,
                };

                const parametersString = Object
                    .keys(parameters)
                    .map(parameterName => `${parameterName}=${encodeURI(parameters[parameterName])}`)
                    .join('&')
                ;

                anchorElement.href = url + '?' + parametersString;
                anchorElement.target = '_blank';
            })
        ;
    })
;
