// just trims every innerHTML so a new line is not seen as a space character
(() => {
    const body = document.querySelector('body');
    body.innerHTML = body.innerHTML.replace(/^\s*|\s*$\s*/gm, '');
})();
